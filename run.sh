#!/bin/bash

# Run Zookeeper
zookeeper-server-start.sh /opt/kafka_2.13-3.4.0/config/zookeeper.properties &

# Run Kafka
kafka-server-start.sh /opt/kafka_2.13-3.4.0/config/server.properties &