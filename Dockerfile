FROM debian

#Copy script to container
COPY run.sh /opt/

# Uptade repositories
RUN apt update -y

# Download and install utils
RUN apt install curl npm -y

# Download and install jdk 17
RUN curl https://download.oracle.com/java/17/archive/jdk-17.0.6_linux-x64_bin.tar.gz --output ./tmp/jdk-17.0.6_linux-x64_bin.tar.gz
RUN tar -xzvf ./tmp/jdk-17.0.6_linux-x64_bin.tar.gz -C ./opt/

# Download and install Kafka 3.4
RUN curl https://downloads.apache.org/kafka/3.4.0/kafka_2.13-3.4.0.tgz --output ./tmp/kafka_2.13-3.4.0.tgz
RUN tar -xvzf ./tmp/kafka_2.13-3.4.0.tgz -C ./opt/

# Export environment variables
ENV JAVA_HOME=/opt/jdk-17.0.6
ENV PATH=$PATH:$JAVA_HOME/bin

ENV KAFKA_HOME=/opt/kafka_2.13-3.4.0
ENV PATH=$PATH:$KAFKA_HOME/bin

# Reload environment variables
RUN bash /etc/profile

# Remove tmp
RUN rm -rf ./tmp

# Expose port Kafka
EXPOSE 9092